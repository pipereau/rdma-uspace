#include <errno.h>
#include <stdio.h>
#include <rdma/rdma_cma.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>

#define RESOLVE_TIMEOUT_MS 5000

struct rdma_sess {
	struct rdma_cm_id *id;
	struct ibv_pd *pd;
	struct ibv_cq *cq;
	struct ibv_qp *qp;
	struct ibv_comp_channel *comp_chan;
	struct rdma_event_channel *cm_channel;
};

static int on_address_resolved(struct rdma_cm_id *id)
{
	int ret;

	ret = rdma_resolve_route(id, RESOLVE_TIMEOUT_MS);
	if (ret < 0) {
		perror("rdma_resolve_route failed");
		return errno;
	}

	return 0;
}

static int on_route_resolved(struct rdma_sess *s)
{
	struct ibv_qp_init_attr	qp_attr = {};
	struct rdma_conn_param conn_param;
	int ret;

	s->pd = ibv_alloc_pd(s->id->verbs);
	if (!s->pd) {
		perror("ibv_alloc_pd failed");
		ret = errno;
		goto create_pd_failed;
	}

	s->comp_chan = ibv_create_comp_channel(s->id->verbs);
	if (!s->comp_chan) {
		perror("ibv_create_comp_channel failed");
		ret = errno;
		goto create_comp_chan_failed;
	}

	s->cq = ibv_create_cq(s->id->verbs, 2, NULL, s->comp_chan, 0);
	if (!s->cq) {
		perror("ibv_create_cq failed");
		ret = errno;
		goto create_cq_failed;
	}

	ret = ibv_req_notify_cq(s->cq, 0);
	if (ret < 0) {
		perror("ibv_req_notify_cq failed");
		ret = errno;
		goto req_notify_cq_failed;
	}

	memset(&qp_attr, 0, sizeof(qp_attr));
	qp_attr.cap.max_send_wr = 2;
	qp_attr.cap.max_send_sge = 1;
	qp_attr.cap.max_recv_wr = 1;
	qp_attr.cap.max_recv_sge = 1;
	qp_attr.send_cq = s->cq;
	qp_attr.recv_cq = s->cq;
	qp_attr.qp_type = IBV_QPT_RC;
	ret = rdma_create_qp(s->id, s->pd, &qp_attr);
	if (ret < 0) {
		perror("rdma_create_qp failed");
		ret = errno;
		goto create_qp_failed;
	}

	memset(&conn_param, 0, sizeof(conn_param));
	conn_param.responder_resources = 1;
	conn_param.initiator_depth = 1;
	conn_param.retry_count = 7;

	ret = rdma_connect(s->id, &conn_param);
	if (ret < 0) {
		perror("rdma_connect failed");
		ret = errno;
		goto connect_failed;
	}

	return 0;

connect_failed:
	rdma_destroy_qp(s->id);
create_qp_failed:
	ibv_destroy_cq(s->cq);
create_cq_failed:
	ibv_destroy_comp_channel(s->comp_chan);
req_notify_cq_failed:
	ibv_destroy_cq(s->cq);
create_comp_chan_failed:
	ibv_dealloc_pd(s->pd);
create_pd_failed:
	return ret;
}

void * event_loop(void *arg)
{
	struct rdma_cm_event *event;
	struct rdma_sess *s = (struct rdma_sess *) arg;
	int ret;

	printf("event_loop\n");

	while (1) {
		ret = rdma_get_cm_event(s->cm_channel, &event);
		if (ret < 0) {
			perror("rdma_get_cm_event failed\n");
			continue;
		}

		switch (event->event) {
		case RDMA_CM_EVENT_ADDR_RESOLVED:
			printf("ADDR RESOLVED\n");
			on_address_resolved(s->id);
			break;

		case RDMA_CM_EVENT_ROUTE_RESOLVED:
			printf("ROUTE RESOLVED\n");
			on_route_resolved(s);
			break;

		case RDMA_CM_EVENT_ESTABLISHED:
			printf("CONNECION ESTABLISHED\n");
			break;

		case RDMA_CM_EVENT_ADDR_ERROR:
		case RDMA_CM_EVENT_ROUTE_ERROR:
		case RDMA_CM_EVENT_CONNECT_ERROR:
		case RDMA_CM_EVENT_UNREACHABLE:
			fprintf(stderr, "Unhandled message type\n");
			break;
		case RDMA_CM_EVENT_REJECTED:
			fprintf(stderr, "Rejected\n");
			break;

		case RDMA_CM_EVENT_DISCONNECTED:
			printf("CLIENT DISCONNECTED\n");
			break;

		case RDMA_CM_EVENT_DEVICE_REMOVAL:
			printf("DEVICE REMOVED\n");
			break;

		default:
			fprintf(stderr, "Bad type\n");
			break;
		}
		rdma_ack_cm_event(event);
	}

	return 0;
}

int main(int argc, char**argv)
{
	struct rdma_sess *s;
	struct sockaddr_in sin;
	pthread_t event_id;
	int port;
	int ret = 0;
	void *res;

	if (argc != 3) {
		printf("Invalid number of arguments. Required=2 Provided=%d\n", argc);
		printf("rdma_server <SERVER_IP> <SERVER_PORT>\n");
		exit(1);
	}

	printf("run client\n");

	s = malloc(sizeof(struct rdma_sess));
	if (!s) {
		ret = ENOMEM;
		goto rdma_sess_alloc_failed;
	}

	s->cm_channel = rdma_create_event_channel();
	if (!s->cm_channel) {
		perror("rdma_create_event_channel failed");
		ret = errno;
		goto event_channel_failed;
	}

	ret = pthread_create(&event_id, NULL, event_loop, s);
	if (ret < 0) {
		fprintf(stderr, "phtread_create failed\n");
		goto thread_create_failed;
	}

	ret = rdma_create_id(s->cm_channel, &s->id, NULL, RDMA_PS_TCP);
	if (ret < 0) {
		perror("rdma_create_id failed");
		ret = errno;
		goto create_id_failed;
	}

	port = atoi(argv[2]);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	inet_pton(AF_INET, argv[1], &sin.sin_addr);

	ret = rdma_resolve_addr(s->id, NULL, (struct sockaddr *) &sin, RESOLVE_TIMEOUT_MS);
	if (ret < 0) {
		perror("rdma_resolve_addr failed");
		ret = errno;
		goto resolve_addr_failed;
	}

	ret = pthread_join(event_id, &res);
	if (ret < 0) {
		fprintf(stderr, "pthread_join failed\n");
	}

	return 0;

resolve_addr_failed:
	rdma_destroy_id(s->id);
create_id_failed:
thread_create_failed:
	rdma_destroy_event_channel(s->cm_channel);
event_channel_failed:
	free(s);
rdma_sess_alloc_failed:
	return ret;
}


